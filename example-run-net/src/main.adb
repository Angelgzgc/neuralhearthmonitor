with Ada.Text_IO;

-- For FFT
with Ada.Numerics.Complex_Arrays;
with Ada.Complex_Text_IO;
with FFT;

-- For Neural Network
with Config;
with NeuralNetwork;
with NTypes;

procedure Main is

   -- FFT
   -- Util reference: http://www.adaic.org/resources/add_content/standards/05rm/html/RM-G-3-2.html
   package CA renames Ada.Numerics.Complex_Arrays;
   function FFT1 is new FFT.Generic_FFT (CA);
   X : CA.Complex_Vector := (1..4 => (1.0, 0.0), 5..8 => (0.0, 0.0));
   Y : CA.Complex_Vector := FFT1 (X);

   -- Neural network
   Net : NeuralNetwork.Neural_Network_Access :=
     new NeuralNetwork.Neural_Network(Layer_Number => Config.Layer_Number,
                                      Input_Number => Config.Input_Number,
                                      Output_Number => Config.Output_Number,
                                      Input_Samples_Size  => 0); -- Not needed
   Input : NTypes.Float_Array := (2.2, 1.0, 3.0, 1.7, 2.3);


begin

   -- Configuration
   Net.Set_Layers(Config.Layers);
   Net.Configure_Run(Config.Weights_File);

   -- Run
   Ada.Text_IO.Put_Line ("       X              FFT X ");
   for I in Y'Range loop
      Ada.Complex_Text_IO.Put (X (I - Y'First + X'First), Aft => 3, Exp => 0);
      Ada.Text_IO.Put (" ");
      Ada.Complex_Text_IO.Put (Y (I), Aft => 3, Exp => 0);
      Ada.Text_IO.New_Line;
   end loop;

   Net.Run(Input);

   -- Output
   Net.Print_Final_Weights;
   Net.Print_Last_Output;
   Net.Pint_Final_Data_Commands;

end Main;
