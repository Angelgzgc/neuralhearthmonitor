# Low-Cost ECG Pathology Detection with Deep Neural Networks

This is the repo for our project "Lowcost ECG Pathology detection with deep neural networks" 
which you can find in here https://www.hackster.io/endlessteam/low-cost-ecg-pathology-detection-with-deep-neural-networks-b417f1

# Directories:

 - Raspberry: Includes the python script to plot the signals received from the STM32.
 - STM32: The Ada code that actually run on the STM32. It also contains some auxiliary functions to use the ADC/Serial...
 - neural-learning: The code that runs on a powerful PC, used to train a neural network and therefore obtains the neural network configuration
 - neural-network-lib: Library to implement a neural network, oriented for PC use with regular IO function.
 - neural-network-lib-stm32: Same library adapted to run in an STM32
