with NTypes;

package NeuralNetwork is

   -----------------------------------------------------------------------------
   -- Neural network type
   -----------------------------------------------------------------------------
   type Neural_Network ( Layer_Number: Natural;
                         Input_Number : Natural; 
                         Output_Number : Natural;
                         Input_Samples_Size : Natural) is tagged private;
   type Neural_Network_Access is access all Neural_Network;

   
   -----------------------------------------------------------------------------
   -- Neural network public functions
   -----------------------------------------------------------------------------
   -- Main network functions
   procedure Set_Layers (This :  access Neural_Network; Layers : in NTypes.Layer_Array); 
   procedure Configure_Train (This : access Neural_Network);     
   procedure Configure_Run (This : access Neural_Network); 
   procedure Run (This : access Neural_Network; Inputs: NTypes.Float_Array);
   
   procedure Clean_Network (This : access Neural_Network);

   -- Functions that return execution errors in training
   function  Total_Error   (This : access Neural_Network; Run_Number: Natural) return Float;
   function  Partial_Error (This : access Neural_Network; Run_Number: Natural) return NTypes.Float_Array;
   
   -- Return result
   function  Get_Output (This : access Neural_Network) return NTypes.Float_Array;
   
private
   
   -----------------------------------------------------------------------------
   -- Neural network private functions
   -----------------------------------------------------------------------------
   -- Configuration procedures
   procedure Generate_Random_Weights (This : access Neural_Network);
   procedure Generate_Random_Bias (This : access Neural_Network);
   procedure Generate_Network (This : access Neural_Network);
   
   -- File operations
   procedure Load_Weights (This : access Neural_Network);
   
   -- Neuron treatment procedures
   procedure Set_Funcs ( N:            in out NTypes.Neuron;
                         Body_Func:    in NTypes.Func;
                         Act_Func :    in NTypes.Func);  
   procedure Set_Inputs  ( N: in out NTypes.Neuron; 
                           Inputs: in NTypes.Float_Array);
   procedure Set_Weights ( N: in out NTypes.Neuron; 
                           Weights: in NTypes.Float_Array);
   procedure Exec_Neuron ( N: in out NTypes.Neuron);
   
   -- Neural network main type   
   type Neural_Network ( Layer_Number: Natural;
                         Input_Number : Natural; 
                         Output_Number : Natural;
                         Input_Samples_Size : Natural) is tagged record    
      
      Net          : NTypes.Network_type(1 .. Layer_Number) := (others => null);
      Inputs_Array : NTypes.Inputs_Array_Type(1 .. Input_Samples_Size) := (others => null);                -- Only for training
      Expected_Outputs_Array : NTypes.Expected_Outputs_Array_Type(1 .. Output_Number) := (others => null); -- Only for training
      Network_Bias : NTypes.Network_Bias_Type(1 .. Layer_Number) := (others => null);
      Layers       : NTypes.Layer_Array_Access := null;
      File_Lines   : NTypes.File_Lines_Type(1 .. Input_Samples_Size);                                      -- Only for training
   end record;


end NeuralNetwork;
