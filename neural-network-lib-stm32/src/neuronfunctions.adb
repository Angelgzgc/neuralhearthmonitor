with Ada.Numerics;
with Ada.Numerics.Generic_Elementary_Functions;

package body NeuronFunctions is

   package F_Numerics is new Ada.Numerics.Generic_Elementary_Functions (Float);
   
   ---------------------------
   -- NEURON BODY FUNCTIONS --
   --------------------------
   
   -----------------------------------------------------------------------------
   -- Summatory body function. This is the standard one
   -----------------------------------------------------------------------------
   function Sum (N: in NTypes.Neuron) return Float is
      Result : Float := 0.0;
   begin 
      for i in 1..N.Input_Size loop
         Result := Result + (N.Inputs(i) * N.In_Weight(i));
      end loop;
      Result := Result + N.Bias;
      return Result;
   end Sum;
 
   
   -------------------------------
   -- NEURON ACTUATOR FUNCTIONS --
   -------------------------------
   
   -----------------------------------------------------------------------------
   -- Sigmoid activation function. Uses the body function output, which has to
   -- be executed first
   -----------------------------------------------------------------------------
   function Sigmoid (N: in NTypes.Neuron) return Float is
      use F_Numerics;
   begin
      return 1.0 / (1.0 + Ada.Numerics.e**(-N.Body_Out));
   end Sigmoid;

end NeuronFunctions;
