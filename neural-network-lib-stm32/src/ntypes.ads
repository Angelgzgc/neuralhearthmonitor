package NTypes is

   -- NOTE: Maximum size for each line of the input file
   Max_Input_File_Line_Size : constant Integer := 300;
   
   ----------------------
   --   NEURON TYPES   --
   ----------------------
   type Neuron;
   type Func is access function (N: in Neuron) return Float;
   type Float_Array is array(Natural range <>) of Float with Default_Component_Value => 0.0;
   type Float_Array_Access is access Float_Array;
   
   type Neuron (Input_Size : Natural) is record
      Inputs      : Float_Array(1 .. Input_Size) := (others => 0.0);
      In_Weight   : Float_Array(1 .. Input_Size) := (others => 0.0);
      Body_Func   : Func;
      Body_Out    : Float   := 0.0;
      Act_Func    : Func;
      Output      : Float   := 0.0;
      Bias        : Float   := 0.0;
      dEtot       : Float   := 0.0; -- Used for the backpropagation function
      dOut        : Float   := 0.0; -- Used for the backpropagation function
   end record;
   
   type Neuron_Access is access all Neuron;

   -- Matrix of (Number of Neurons on layer, Number of inputs for each neuron)
   type Neuron_Weights is array (Natural range <>, Natural range <>) of Float;
   type Neuron_Weights_Access is access all Neuron_Weights;
   
   -- TODO: Necessary to add support for different functions in the same layer?
   type Neuron_Funcs is array (Natural range <>) of Func;  
   
   type Bias_Type is array(Natural range <>) of float;
   type Bias_Access is access all Bias_Type;
   
   -- Array with the bias of the network
   type Network_Bias_Type is array (natural range <>) of Bias_Access;
   
     
   ----------------------
   --    LAYER TYPES   --
   ----------------------      
   -- Configuration of each layer
   type Layer_Config is record
      Size      : Natural := 0;  -- Number of neurons on each layer
      Input_num : Natural := 0;  -- Number of inputs for each neuron on this layer
      Body_Func : Func := null;  -- Body function asigned to this layer
      Act_Func  : Func := null;  -- Actuator function asigned to this layer
      Weights   : Neuron_Weights_Access  := null; -- Access to matrix of the neuron's weights
      Bias      : Bias_Access            := null; -- Acces to array of Bias
   end record;
   
   type Layer_Array is array (Natural range <>) of Layer_Config;
   type Layer_Array_Access is access Layer_Array;
   
   -- Array with the neurons of each layer
   type Net_layer    is array(Natural range <>) of Neuron_Access;
   type Layer_Access is access all Net_layer;
   
   type Inputs_Array_Type is array(Natural range <>) of Float_Array_Access; 
   type Expected_Outputs_Array_Type is array (Natural range <>) of Float_Array_Access;
   
   
   ----------------------
   --  NETWORK TYPES   --
   ----------------------
   -- Array with the layers of the network
   type Network_type is array(Natural range <>) of Layer_Access;
    

   ----------------------
   --    FILE TYPES    --
   ----------------------
   type File_Lines_Type is array(Natural range <>) of String(1 .. Max_Input_File_Line_Size);

   type String_range_type is record
      First : Integer;
      Last  : Integer;
   end record;
   
end NTypes;
