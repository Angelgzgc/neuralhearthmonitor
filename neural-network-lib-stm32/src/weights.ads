with NTypes;

package Weights is

   type Neuron_Weights_Array is array (Natural range <>) of NTypes.Neuron_Weights_Access;

   -- Weights taken from the Neural Network training
   -- By now, for STM32 this part must be configured manually
   -- For future improves, adapt the code to read from file through File.IO
   WeightsL1 : aliased NTypes.Neuron_Weights := ((0.71000,  0.17000,  0.45000,  0.63000,  0.92000),
                                                 (0.86000,  0.13001,  0.39000,  0.22000,  0.98000),
                                                 (0.18000,  0.73000,  0.88000,  0.41000,  0.59000),
                                                 (0.90000,  0.37000,  0.78000,  0.82000,  0.20000),
                                                 (0.87000,  0.41000,  0.93000,  0.10000,  0.78000),
                                                 (0.87000,  0.85000,  0.89000,  0.60000,  0.05000),
                                                 (0.08000,  0.71000,  0.92000,  0.10000,  0.93000),
                                                 (0.88000,  0.24000,  0.49000,  0.76000,  0.56000),
                                                 (0.53000,  0.55000,  0.84000,  0.47000,  0.68000),
                                                 (0.80001,  0.36004,  0.21001,  0.04001,  0.18001),
                                                 (0.40000,  0.33001,  0.30000,  0.99000,  0.18000),
                                                 (0.59000,  0.46000,  0.29000,  0.73000,  0.95000),
                                                 (0.02000,  0.49001,  0.71000,  0.04000,  0.17000),
                                                 (0.49001,  0.03013,  0.37002,  0.77001,  0.31001),
                                                 (0.90000,  0.39000,  0.75000,  0.18000,  0.42000));

   WeightsL2 : aliased NTypes.Neuron_Weights := ((0.84029,  0.44034,  0.38034,  0.39034,  0.23039,  0.50029,  0.31034,  0.13039,  0.64029,  0.51029,  0.73029,  0.70029,  0.72029,  0.99029,  0.18039),
                                                 (0.44998,  0.12002,  0.60997,  0.92997,  0.52997,  0.71997,  0.95997,  0.44998,  0.56997,  0.18000,  0.91997,  0.51997,  0.41998,  0.79997,  0.28998),
                                                 (0.43999,  0.00006,  0.95996,  0.92996,  0.89996,  0.44999,  0.01006,  0.51996,  0.60996,  0.54996,  0.43999,  0.22002,  0.72996,  0.23002,  0.77996),
                                                 (0.50008,  0.56008,  0.55008,  0.70008,  0.17027,  0.81008,  0.27020,  0.19027,  0.72008,  0.55008,  0.92008,  0.12025,  0.11024,  0.00025,  0.58008),
                                                 (0.77009,  0.94009,  0.25013,  0.87009,  0.99009,  0.13017,  0.71009,  0.33013,  0.20017,  0.54009,  0.07021,  0.50009,  0.73009,  0.86009,  0.11021),
                                                 (0.26089,  0.04093,  0.76077,  0.00093,  0.03093,  0.76077,  0.28090,  0.56077,  0.65077,  0.66077,  0.92077,  0.77077,  0.69077,  0.17094,  0.43090),
                                                 (0.46045,  0.22052,  0.46045,  0.25045,  0.05053,  0.42045,  0.43045,  0.83038,  0.53038,  0.76038,  0.35045,  0.90038,  0.44045,  0.69038,  0.63038),
                                                 (0.59090,  0.76090,  0.29106,  0.20106,  0.04106,  0.35106,  0.87091,  0.67090,  0.05106,  0.78090,  0.66090,  0.47106,  0.35106,  0.17105,  0.68090),
                                                 (0.05017,  0.60008,  0.67008,  0.38010,  0.27010,  0.62008,  0.96008,  0.90008,  0.65008,  0.70008,  0.73008,  0.39010,  0.99008,  0.40010,  0.12015),
                                                 (0.94089,  0.15106,  0.50089,  0.90089,  0.43103,  0.13106,  0.35103,  0.09105,  0.58089,  0.88089,  0.27103,  0.43103,  0.64089,  0.03104,  0.68089));

   WeightsL3 : aliased NTypes.Neuron_Weights := ((-0.17371, -0.49400, -0.53375, -1.07288, -1.04385, -0.85317, -0.56354, -0.51311, -0.57399, -0.91319),
                                                 (-1.08167, -0.65195, -0.51171, -0.38088, -0.30180, -1.00115, -0.75151, -0.99110, -0.65194, -0.41117),
                                                 (0.18874,  0.31878,  0.31875,  0.27863,  1.00876,  0.93867,  0.76872,  1.07866,  0.82878,  1.02867),
                                                 (0.89681,  0.84683,  0.12682,  0.93676,  0.43682,  0.51678,  0.36680,  1.04678,  0.72683,  0.95678),
                                                 (-1.03864, -0.44892, -0.56869, -0.15785, -0.54878, -0.41812, -0.88849, -0.79807, -0.82892, -1.03814));

   WeightsArray : Neuron_Weights_Array(1..3) := (WeightsL1'Access,
                                                 WeightsL2'Access,
                                                 WeightsL3'Access);

end Weights;
