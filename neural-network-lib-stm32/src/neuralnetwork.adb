with Ada.Numerics.Float_Random;
with Ada.Characters.Latin_1;
with Weights;

package body NeuralNetwork is

   -----------------------------------------------------------------------------
   -- Sets the layer data
   -----------------------------------------------------------------------------
   procedure Set_Layers (This : access Neural_Network; Layers : in NTypes.Layer_Array) is
   begin
      This.Layers := new NTypes.Layer_Array(Layers'Range);
      This.Layers.all := Layers;
   end Set_Layers;
   
   
   -----------------------------------------------------------------------------
   -- Call the necessary functions to set up the neural network. Layers must be
   -- configured previously with Set_layers. Inputs must be read before.
   -----------------------------------------------------------------------------
   procedure Configure_Train (This : access Neural_Network) is
   begin
      This.Generate_Random_Bias;
      This.Generate_Random_Weights;
      This.Generate_Network;
   end Configure_Train;
   
   -----------------------------------------------------------------------------
   -- Call the necessary functions to set up the neural network. Layers must be
   -- configured previously with Set_layers. As inputs takes the weights file
   -- name.
   -----------------------------------------------------------------------------
   procedure Configure_Run (This : access Neural_Network) is
   begin
      This.Generate_Random_Bias;
      This.Generate_Random_Weights;
      This.Load_Weights;
      This.Generate_Network;
   end Configure_Run;
   
   -----------------------------------------------------------------------------
   -- Generates random weights for every neuron in the network. Should be called
   -- in training and production mode.
   -----------------------------------------------------------------------------
   procedure Generate_Random_Weights (This : access Neural_Network) is
      Gen : Ada.Numerics.Float_Random.Generator;
   begin
      for L in This.Layers.all'Range loop  -- For each layer
         This.Layers.all(L).Weights := new NTypes.Neuron_Weights(1 .. This.Layers.all(L).Size, 1 .. This.Layers.all(L).Input_num);      
         for N in 1..This.Layers.all(L).Size loop  -- For each neuron in layer  
            for W in 1 .. This.Layers.all(L).Input_num loop  -- For each weigth in neuron
               This.Layers.all(L).Weights.all(N,W) := Ada.Numerics.Float_Random.Random(Gen);
            end loop;             
         end loop;
      end loop;  
   end Generate_Random_Weights;


   -----------------------------------------------------------------------------
   -- Generates random bias for every neuron in the network. Should be called
   -- in training and production mode.
   -- TODO: Bias implemented but not used by now, not taken in account on the 
   -- backpropagation algorithm, so are less than useless, by now is filled  
   -- with 0s. Neither have support for read from file or write to it at the end
   -----------------------------------------------------------------------------
   procedure Generate_Random_Bias (This : access Neural_Network) is
   begin
      for L in This.Layers.all'Range loop  -- For each layer
         This.Layers.all(L).Bias := new NTypes.Bias_Type(1 .. This.Layers.all(L).Size);      
         for N in 1..This.Layers.all(L).Size loop  -- For each neuron in layer  
            This.Layers.all(L).Bias.all(N) := 0.0;
         end loop;
      end loop;  
   end Generate_Random_Bias;
   
   
   -----------------------------------------------------------------------------
   -- Generate the neural network for run it. 
   -- This procedure fills the array of neurons with functions, inputs, bias 
   -- and weights.
   -----------------------------------------------------------------------------
   procedure Generate_Network (This : access Neural_Network) is
   begin
      for L in This.Layers.all'Range loop -- For each layer
         
         This.Net(L) := new NTypes.Net_layer(1 .. This.Layers.all(L).Size);
         
         for N in 1 .. This.Layers.all(L).Size loop -- For each neuron
            
            -- In the first  layer, the number of inputs for the neuron are equal
            -- to the number of inputs of the network
            if L = 1 then
               This.Net(L).all(N) := new NTypes.Neuron(This.Input_Number);
            else
               This.Net(L).all(N) := new NTypes.Neuron(This.Layers.all(L-1).Size);
            end if;
            -- Initialize the neuron functions
            Set_Funcs(N         => This.Net(L).all(N).all,
                      Body_Func => This.Layers.all(L).Body_Func,
                      Act_Func  => This.Layers.all(L).Act_Func);
            
            -- Set the neuron weights
            for W in 1..This.Layers.all(L).Input_num loop
               This.Net(L).all(N).all.In_Weight(W) := This.Layers.all(L).Weights.all(N, W); 
            end loop;

            This.Net(L).all(N).all.Bias := This.Layers.all(L).Bias.all(N);
         end loop;      
      end loop;
   end Generate_Network;
   
   
   -----------------------------------------------------------------------------
   -- Clean the neural network's neurons information.
   -- Needs to be filled fully again.
   -----------------------------------------------------------------------------
   procedure Clean_Network (This : access Neural_Network) is
   begin
      for L in This.Layers.all'Range loop      
         for N in 1..This.Layers.all(L).Size loop
            This.Net(L).all(N).Body_Out    := 0.0;
            This.Net(L).all(N).Output      := 0.0;
            This.Net(L).all(N).Inputs      := (others => 0.0);
         end loop;
      end loop;
   end Clean_Network;
   
   
   -----------------------------------------------------------------------------
   -- Run the neural network with the given parameters, setted previously by
   -- the Generate_Network procedure. This calculates the output values for
   -- the neural network
   -----------------------------------------------------------------------------
   procedure Run (This : access Neural_Network; Inputs: NTypes.Float_Array) is
   begin
      pragma Assert(Inputs'Length = This.Input_Number, "Input size error in run");
      for L in This.Layers.all'Range loop -- For each layer     
         -- Fill the neuron input data and execute it
         if L = 1 then -- First layer
            for N in 1 .. This.Layers.all(L).Size loop -- For each neuron
               Set_Inputs(N      => This.Net(L).all(N).all,
                          Inputs => Inputs);
               Exec_Neuron(This.Net(L).all(N).all);
            end loop;          
         else -- No first layer
            -- The inputs for the mid layers are the output of the last layer
            for N in 1..This.Layers.all(L).Size loop -- For each neuron
               for I in 1..This.Layers.all(L-1).Size loop -- For each neuron's input
                  This.Net(L).all(N).Inputs(I) := This.Net(L-1).all(I).Output;
               end loop;                  
               -- We execute each neuron
               Exec_Neuron(This.Net(L).all(N).all);
            end loop;
         end if;             
      end loop;
   end Run;
   
   -----------------------------------------------------------------------------
   -- Calculates the total error given by a neural network run, in comparison
   -- with the expected output values read in the input file
   -----------------------------------------------------------------------------
   function Total_Error (This : access Neural_Network; Run_Number: Natural) return Float is
      Total_Error    : Float := 0.0;
      Neuron_Error   : Float := 0.0;
      Expected_Value : Float := 0.0;
      Neuron_Output  : Float := 0.0;
   begin
      for i in 1 .. This.Output_Number loop
         Neuron_Output  := This.Net(This.Layers.all'Last).all(i).Output;
         Expected_Value := This.Expected_Outputs_Array(Run_Number)(i);
         Neuron_Error   := (( Expected_Value - Neuron_Output ) ** 2 ) / 2.0;
         Total_Error    := Total_Error + Neuron_Error;
      end loop;
      return Total_Error;
   end Total_Error;
   
   
   -----------------------------------------------------------------------------
   -- Calculates the error independent for every single output, in comparison
   -- with the expected output values read in the input file. Returns an array.
   -----------------------------------------------------------------------------
   function Partial_Error (This : access Neural_Network; Run_Number: Natural) return NTypes.Float_Array is
      Partial_Error  : NTypes.Float_Array(1 .. This.Output_Number) := (others => 0.0);
      Neuron_Error   : Float := 0.0;
      Expected_Value : Float := 0.0;
      Neuron_Output  : Float := 0.0;
   begin
      for i in 1 .. This.Output_Number loop
         Neuron_Output    := This.Net(This.Layers.all'Last).all(i).Output;
         Expected_Value   := This.Expected_Outputs_Array(Run_Number)(i);
         Neuron_Error     := (( Expected_Value - Neuron_Output ) ** 2 ) / 2.0;
         Partial_Error(i) := Neuron_Error;
      end loop;
      return Partial_Error;
   end Partial_Error;
   
   
   -----------------------------------------------------------------------------
   -- Sets the body and actuator functions for a neuron
   -----------------------------------------------------------------------------
   procedure Set_Funcs ( N:            in out NTypes.Neuron;
                         Body_Func:    in NTypes.Func;
                         Act_Func :    in NTypes.Func) is
   begin
      N.Body_Func := Body_Func;
      N.Act_Func  := Act_Func;
   end Set_Funcs;
      
   
   -----------------------------------------------------------------------------
   -- Executes the body and actuator functions for a neuron
   -----------------------------------------------------------------------------
   procedure Exec_Neuron ( N: in out NTypes.Neuron) is
   begin
      N.Body_Out := N.Body_Func.all(N);
      N.Output   := N.Act_Func.all(N);
   end Exec_Neuron;   

   
   -----------------------------------------------------------------------------
   -- Sets a neuron weights
   -----------------------------------------------------------------------------
   procedure Set_Weights ( N: in out NTypes.Neuron; 
                           Weights: in NTypes.Float_Array) is
   begin
      N.In_Weight := Weights;
   end Set_Weights;
   
   
   -----------------------------------------------------------------------------
   -- Sets a neuron inputs
   -----------------------------------------------------------------------------
   procedure Set_Inputs ( N: in out NTypes.Neuron; 
                          Inputs: in NTypes.Float_Array) is
   begin
      N.Inputs      := Inputs;
   end Set_Inputs;
   
 
   -----------------------------------------------------------------------------
   -- Prints the output of the last run
   -----------------------------------------------------------------------------
   function Get_Output (This : access Neural_Network) return NTypes.Float_Array is 
      Aux  : Float   := 0.0;
      Last : Natural := This.Layers.all'Last;
      Ret  : NTypes.Float_Array(1 .. This.Layers.all(Last).Size);
   begin
      for N in 1 .. This.Layers.all(Last).Size loop    -- For each neuron in the last layer
         Ret(N) := This.Net(Last).all(N).Output;
      end loop;
      return Ret;
   end Get_Output;
   
   -----------------------------------------------------------------------------
   -- Loads the weight file given in weights.adb and previously generated by the 
   -- Write_Outputs function. Used for production mode, not learning.
   -----------------------------------------------------------------------------
   procedure Load_Weights (This : access Neural_Network) is
   begin            
      for L in This.Layers.all'Range loop
         This.Layers.all(L).Weights.all := Weights.WeightsArray(L).all;
      end loop;  
   end Load_Weights;

end NeuralNetwork;
