
-- For FFT
with Ada.Numerics.Complex_Arrays;
with FFT;

with NTypes;

package ECGprocess is

   -- FFT
   -- Util reference: http://www.adaic.org/resources/add_content/standards/05rm/html/RM-G-3-2.html
   package CA renames Ada.Numerics.Complex_Arrays;
   procedure FFT1 is new FFT.Generic_FFT (CA);
   
   function processECG (Data: CA.Complex_Vector) return NTypes.Float_Array;
   function calculateFFTModule (Data : CA.Complex_Vector) return NTypes.Float_Array;
     
end ECGprocess;
