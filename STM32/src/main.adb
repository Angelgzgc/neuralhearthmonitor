with Ada.Real_Time;      use Ada.Real_Time;
with STM32.Board;        use STM32.Board;
with Console;
with ecgreader;
with ecgprocess;

with Ada.Numerics.Complex_Arrays;
with Ada.Numerics.Complex_Types;

-- For Neural Network
with Config;
with NeuralNetwork;
with NTypes;

with Ada.Exceptions;

procedure Main is

   Next_execution: Ada.Real_Time.Time;
   Period: constant Ada.Real_Time.Time_Span:= Ada.Real_Time.To_Time_Span(0.02); -- 20ms

   -- Maximum processing chunk size of 15 to use unbounded return on functions,
   -- due to the high use of the stack by the FFT function, and a bug, that produces STORAGE_ERROR. HINT:
   -- https://stackoverflow.com/questions/26829681/ada-fixed-stringss-stm32-arm/26919022
   -- FIX: More than 128(included) crash
   -- NOTE: Must be power of 2 for FFT
   ADC_data_chunk : constant Natural := 64;

   -- Neural network
   Net : constant NeuralNetwork.Neural_Network_Access :=
     new NeuralNetwork.Neural_Network(Layer_Number => Config.Layer_Number,
                                      Input_Number => Config.Input_Number,
                                      Output_Number => Config.Output_Number,
                                      Input_Samples_Size  => 0); -- Not needed
   NetInput : NTypes.Float_Array(1..Config.Input_Number);

begin
   Next_execution:= Ada.Real_Time.Clock + Period;
   Initialize_LEDs;
   Console.Init(115200);
   ecgreader.Init;

   ------------------------------------------
   --     Neural Network Configuration     --
   ------------------------------------------
   Net.Set_Layers(Config.Layers);
   Net.Configure_Run;
   ------------------------------------------

   loop

      ECGReader.updateData;

      if ECGReader.dataLen > 1 then
         Console.putLine("ADC:" & Float'Image(Float(ECGReader.data(ECGReader.dataLen-1))));
      end if;

      if ECGReader.dataLen >= ADC_data_chunk then
         -- Prepare data
         declare
            X    : Ada.Numerics.Complex_Arrays.Complex_Vector(1..ECGReader.dataLen);
            Comp : Ada.Numerics.Complex_Types.Complex := (0.0, 0.0);
         begin
            for i in 1..ADC_data_chunk loop
               Comp.Re := Float(ECGReader.data(i));
               X(i) := Comp;
            end loop;

            -- Process read data and output parameters
            NetInput := ECGprocess.processECG(X);
         end;

         -- Neural Network Run
         Net.Run(NetInput);

         -- Send output to raspberry
         declare
            Output : constant NTypes.Float_Array := Net.Get_Output;
         begin
            for N in Output'Range loop
               Console.putLine("NET" & N'Image & ":" & Output(N)'Image);
            end loop;
         end;

         -- Reset data
         ECGReader.data(1..ECGReader.Max_data_len-ADC_data_chunk) := ECGReader.data(ADC_data_chunk+1..ECGReader.Max_data_len);
         ECGReader.dataLen := ECGReader.dataLen - ADC_data_chunk;

      end if;


      -- Unused console echo example
--        declare
--           c : String(1..20);
--           len : Integer;
--        begin
--           if Console.isStringReady then
--              Console.getString(c, len);
--              Console.putLine("Recv: " & c(1..len));
--              STM32.board.Toggle_LEDs(STM32.board.All_LEDs);
--           end if;
--        end;

      delay until Next_execution;
      Next_execution:= clock + Period;

   end loop;

exception
   when error : others =>
      Console.put(Ada.Exceptions.Exception_Information(error) & ASCII.CR & ASCII.LF);

end Main;
