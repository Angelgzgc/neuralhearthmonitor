with HAL;
with STM32.GPIO;
with STM32.ADC;
with STM32.device;

package ECGReader is

   -- ECGReader configuration
   Max_data_len : constant Natural := 1_000;
   adc_pin : STM32.GPIO.GPIO_Point := STM32.device.PA4;
   dev : STM32.ADC.Analog_To_Digital_Converter renames STM32.device.ADC_1;
   adc_conv: STM32.ADC.Regular_Channel_Conversions(1..1) := (others => (Channel => 4,
                                                            Sample_Time => STM32.ADC.Sample_56_Cycles));
   
   -- Types and data   
   type ADCValue is delta (1.0/(2**12-1)) range 0.0 .. 3.3;
   for ADCValue'Small use (1.0/(2**12-1));
   
   type ECGDataType is array (Integer range <>) of ADCValue;
   
   data : ECGDataType(1..Max_data_len);
   dataLen : Integer := 0;
   bufferFull : Boolean := False;
   
   -- Initialize GPIO pins and ADC Module.
   procedure Init;
   
   function getData return HAL.Uint16;
   procedure updateData;
      
end ECGReader;
