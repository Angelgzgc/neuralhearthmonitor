with NTypes;
with NeuronFunctions;

package Config is
   
   -------------------------
   -- Configuration
   -------------------------    
   Input_Number        : constant := 5;     -- Quantity of numbers (columns) of the inputs 
   Output_Number       : constant := 5;    -- Number of outputs of the network
   
   -------------------------
   -- Weights file
   -------------------------
   -- Unused on STM32
   -- Weights_File          : constant String := "weights.dat"; -- File of weight inputs
   
   ----------------------
   --      LAYERS      --
   ----------------------
   -- Input layer not taken into account, only hidden and output layers
   Layer_Number        : constant := 3;     -- Number of layers of the network
   
   -- Number of neurons for each layer
   L1_size : constant := 15;
   L2_size : constant := 10;
   L3_size : constant := Output_Number;
   
   Layer1 : NTypes.Layer_Config := NTypes.Layer_Config'(Size      => L1_size,
                                                        Input_num => Input_Number,  -- NOTE: The first layer always has this input number
                                                        Body_Func => NeuronFunctions.Sum'Access,
                                                        Act_Func  => NeuronFunctions.Sigmoid'Access,
                                                        Weights   => null,
                                                        Bias      => null);
   Layer2 : NTypes.Layer_Config := NTypes.Layer_Config'(Size      => L2_size,
                                                        Input_num => L1_size,
                                                        Body_Func => NeuronFunctions.Sum'Access,
                                                        Act_Func  => NeuronFunctions.Sigmoid'Access,
                                                        Weights   => null,
                                                        Bias      => null);
   Layer3 : NTypes.Layer_Config := NTypes.Layer_Config'(Size      => L3_size,
                                                        Input_num => L2_size,
                                                        Body_Func => NeuronFunctions.Sum'Access,
                                                        Act_Func  => NeuronFunctions.Sigmoid'Access,
                                                        Weights   => null,
                                                        Bias      => null);
   
   Layers : NTypes.Layer_Array(1..Layer_Number) := (Layer1,
                                                    Layer2,
                                                    Layer3);
   
end Config;
