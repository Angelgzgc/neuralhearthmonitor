
with Ada.Numerics.Elementary_Functions;
with Config;

package body ECGprocess is

   package EF renames Ada.Numerics.Elementary_Functions;
   
   function processECG (Data: CA.Complex_Vector) return NTypes.Float_Array is
      FFTData   : CA.Complex_Vector(Data'Range);
      FFTModule : NTypes.Float_Array(Data'Range);
      Output    : NTypes.Float_Array(1 .. Config.Input_Number);
      Max       : Float := 0.0;        -- Maximum value of the sample
      Min       : Float := Float'Last; -- Minimum value of the sample      
      Mean      : Float := 0.0;        -- Mean of energy samples
      Var       : Float := 0.0;        -- Variance of energy samples
      HaPo      : Float := 0.0;        -- Half Point of the function energy
   begin
      
      FFT1 (Data, FFTData);
      FFTModule := calculateFFTModule(FFTData);
      
      -- Max
      for i in FFTModule'Range loop
         if FFTModule(i) > Max then Max := FFTModule(i); end if;
      end loop;
      
      -- Min
      for i in FFTModule'Range loop
         if FFTModule(i) < Min then Min := FFTModule(i); end if;
      end loop;
      
      -- Mean
      for i in FFTModule'Range loop
         Mean := Mean + FFTModule(i);
      end loop;
      if FFTModule'Length > 0 then 
         Mean := Mean / Float(FFTModule'Length);
      end if;
        
      -- Var
      for i in FFTModule'Range loop
         Var := Var + ((FFTModule(i) - Mean) ** 2);
      end loop;
      if FFTModule'Length - 1 > 0 then 
         Var := EF.Sqrt(Var / Float(FFTModule'Length - 1));
      end if;
      
      -- HaPo (frequency that divides up the spectrum into two parts of the same area)
      -- https://apps.dtic.mil/dtic/tr/fulltext/u2/a411396.pdf
      declare
         Half  : constant Float := (Mean * Float(FFTModule'Length)) / 2.0;
         Acum  : Float := 0.0;
         Count : Integer := 0;
      begin
         while Acum < Half  and Count <= FFTModule'Length loop            
            Count := Count + 1;
            Acum  := Acum + FFTModule(Count);
         end loop;
         HaPo := Float(Count);
      end;

      Output(1) := Max;
      Output(2) := Min;
      Output(3) := Mean;
      Output(4) := Var;
      Output(5) := HaPo;
      
      return Output;
   end;
   
   function calculateFFTModule (Data : CA.Complex_Vector) return NTypes.Float_Array is
      Ret  : NTypes.Float_Array(Data'Range);
   begin
      for i in Data'Range loop
         if Data(i).Re'Valid and Data(i).Im'Valid then
            Ret(i) := EF.Sqrt((Data(i).Re ** 2) + (Data(i).Im ** 2));
         elsif Data(i).Re'Valid then
            Ret(i) := Data(i).Re;
         else 
            Ret(i) := 0.0;
         end if;
      end loop;
      return Ret;
   end;

end ECGprocess;
