with Ada.Numerics.Generic_Complex_Arrays;

-- FFT code from: https://rosettacode.org/wiki/Fast_Fourier_transform#Ada
package FFT is

   generic
      with package Complex_Arrays is
        new Ada.Numerics.Generic_Complex_Arrays (<>);
      use Complex_Arrays;

   procedure Generic_FFT (X : Complex_Vector; Y : out Complex_Vector);

end FFT;
