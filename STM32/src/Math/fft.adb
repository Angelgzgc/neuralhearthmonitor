with Ada.Numerics;
with Ada.Numerics.Generic_Complex_Elementary_Functions;
with Ada.Exceptions;
with Console;

package body FFT is
   
   procedure Generic_FFT (X : Complex_Vector; Y : out Complex_Vector) is
 
      package Complex_Elementary_Functions is
        new Ada.Numerics.Generic_Complex_Elementary_Functions
          (Complex_Arrays.Complex_Types);
 
      use Ada.Numerics;
      use Complex_Elementary_Functions;
      use Complex_Arrays.Complex_Types;
      
      procedure FFT (X : Complex_Vector; N, S : Positive; Y : out Complex_Vector) is
      begin
         
         if N = 1 then
            Y := (1..1 => X (X'First));
         else
            declare
               F : constant Complex  := exp (Pi * j / Real_Arrays.Real (N/2));
               Even : Complex_Vector(1..N/2);-- := FFT (X, N/2, 2*S);
               Odd  : Complex_Vector(1..N/2);-- := FFT (X (X'First + S..X'Last), N/2, 2*S);
            begin
               FFT (X, N/2, 2*S, Even);
               FFT (X (X'First + S..X'Last), N/2, 2*S, Odd);
               
               for K in 0..(N/2 - 1) loop
                  declare
                     T : constant Complex := Odd (Odd'First + K) / F ** K;
                  begin
                     Odd  (Odd'First  + K) := Even (Even'First + K) - T;
                     Even (Even'First + K) := Even (Even'First + K) + T;
                  end;
               end loop;
               Y := Even & Odd;
               --return Even & Odd;
            end;
         end if;
      end FFT;
      
   begin
      FFT(X, X'Length, 1, Y);
      --return FFT(X, X'Length, 1);
      
   exception
      when error : others =>
         Console.put(Ada.Exceptions.Exception_Information(error) & ASCII.CR & ASCII.LF); 
   end Generic_FFT;
      
end FFT;
