with STM32.board;

package body ECGReader is

   procedure Init is
      config: STM32.GPIO.GPIO_Port_Configuration(STM32.GPIO.Mode_Analog);
   begin
      
      -- Configure GPIOs
      config.Resistors := STM32.GPIO.Floating;
      
      STM32.device.Enable_Clock(adc_pin);
      STM32.GPIO.Configure_IO(adc_pin, config);
      
      
      -- Configure ADC
      STM32.device.Enable_Clock(dev);
      STM32.device.Reset_All_ADC_Units;
      STM32.board.Turn_On(stm32.board.Red_LED);
      STM32.ADC.Enable(dev);
      --
      stm32.board.All_LEDs_On;
      STM32.ADC.Configure_Unit(This       => dev,
                               Resolution => STM32.ADC.ADC_Resolution_12_Bits,
                               Alignment  => STM32.ADC.Right_Aligned);
      
      STM32.ADC.Configure_Regular_Conversions(This        => dev,
                                              Continuous  => False,
                                              Trigger     => STM32.ADC.Software_Triggered,
                                              Enable_EOC  => True ,
                                              Conversions => adc_conv);
      STM32.ADC.Start_Conversion(dev);
   end Init;
   
   function getData return HAL.UInt16 is
   begin
      STM32.ADC.Start_Conversion(dev);
      while not STM32.ADC.Status(dev, STM32.ADC.Regular_Channel_Conversion_Complete)
      loop
         null;
      end loop;
      return STM32.ADC.Conversion_Value(dev);
   end getData;
   
   procedure updateData is
   begin
      dataLen := dataLen + 1;
      data(dataLen) := ADCValue(Float(getData)*3.3/4096.0);
   end updateData;

end ECGReader;
