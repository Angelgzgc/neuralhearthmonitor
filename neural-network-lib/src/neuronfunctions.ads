with NTypes;

package NeuronFunctions is

   ----------------------
   -- NEURON FUNCTIONS --
   ----------------------  
   -- Body functions
   function Sum (N: in NTypes.Neuron) return Float;

   -- Activation functions
   -- TODO: Add more activation functions
   function Sigmoid (N: in NTypes.Neuron) return Float;

end NeuronFunctions;
