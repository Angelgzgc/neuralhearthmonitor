with NTypes;

package NeuralNetwork is

   -----------------------------------------------------------------------------
   -- Neural network type
   -----------------------------------------------------------------------------
   type Neural_Network ( Layer_Number: Natural;
                         Input_Number : Natural; 
                         Output_Number : Natural;
                         Input_Samples_Size : Natural) is tagged private;
   type Neural_Network_Access is access all Neural_Network;

   
   -----------------------------------------------------------------------------
   -- Neural network public functions
   -----------------------------------------------------------------------------
   -- Main network functions
   procedure Set_Layers (This :  access Neural_Network; Layers : in NTypes.Layer_Array); 
   procedure Configure_Train (This : access Neural_Network);     
   procedure Configure_Run (This : access Neural_Network; Weight_File : String);
   procedure Train (This : access Neural_Network; Iteration_Number: Natural; Learning_Rate : Float);   
   procedure Run (This : access Neural_Network; Inputs: NTypes.Float_Array);
   
   procedure Clean_Network (This : access Neural_Network);

   -- Functions that return execution errors in training
   function  Total_Error   (This : access Neural_Network; Run_Number: Natural) return Float;
   function  Partial_Error (This : access Neural_Network; Run_Number: Natural) return NTypes.Float_Array;
   
   -- File operations
   procedure Read_Inputs (This : access Neural_Network; File : String);
   procedure Write_Weights (This : access Neural_Network; File : String);   

   -- Functions to print and return results
   procedure Print_Final_Weights (This : access Neural_Network);
   procedure Pint_Final_Data_Commands (This : access Neural_Network);
   procedure Print_Last_Output (This : access Neural_Network);
   function  Get_Output (This : access Neural_Network) return NTypes.Float_Array;
   
private
   
   -----------------------------------------------------------------------------
   -- Neural network private functions
   -----------------------------------------------------------------------------
   -- Configuration procedures
   procedure Generate_Random_Weights (This : access Neural_Network);
   procedure Generate_Random_Bias (This : access Neural_Network);
   procedure Generate_Network (This : access Neural_Network);
   
   -- Backpropagation function for training
   procedure Backpropagation (This : access Neural_Network; Run_Number: Natural; Learning_Rate : Float);
   
   -- File operations
   function Split( Source  : in String; 
                   Pattern : in Character;
                   Field   : in Natural) return NTypes.String_range_type; 
   procedure Process_File (This : access Neural_Network); 
   procedure Load_Weights (This : access Neural_Network; File : String);
   
   -- Neuron treatment procedures
   procedure Set_Funcs ( N:            in out NTypes.Neuron;
                         Body_Func:    in NTypes.Func;
                         Act_Func :    in NTypes.Func);  
   procedure Set_Inputs  ( N: in out NTypes.Neuron; 
                           Inputs: in NTypes.Float_Array);
   procedure Set_Weights ( N: in out NTypes.Neuron; 
                           Weights: in NTypes.Float_Array);
   procedure Exec_Neuron ( N: in out NTypes.Neuron);
   
   -- Auxiliar procedures to print results
   procedure Print_Layer (This : access Neural_Network; L : Natural);
   procedure Print_Neuron (This : access Neural_Network; L : Natural; N : Natural);
   procedure Print_Train_Outputs (This : access Neural_Network);
   
   -- Neural network main type   
   type Neural_Network ( Layer_Number: Natural;
                         Input_Number : Natural; 
                         Output_Number : Natural;
                         Input_Samples_Size : Natural) is tagged record    
      
      Net          : NTypes.Network_type(1 .. Layer_Number) := (others => null);
      Inputs_Array : NTypes.Inputs_Array_Type(1 .. Input_Samples_Size) := (others => null);                -- Only for training
      Expected_Outputs_Array : NTypes.Expected_Outputs_Array_Type(1 .. Output_Number) := (others => null); -- Only for training
      Network_Bias : NTypes.Network_Bias_Type(1 .. Layer_Number) := (others => null);
      Layers       : NTypes.Layer_Array_Access := null;
      File_Lines   : NTypes.File_Lines_Type(1 .. Input_Samples_Size);                                      -- Only for training
   end record;


end NeuralNetwork;
