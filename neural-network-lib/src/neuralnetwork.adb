with Ada.Numerics.Float_Random;
with Ada.Characters.Latin_1;
with Ada.Text_IO;
with Ada.Float_Text_IO;

package body NeuralNetwork is

   -----------------------------------------------------------------------------
   -- Sets the layer data
   -----------------------------------------------------------------------------
   procedure Set_Layers (This : access Neural_Network; Layers : in NTypes.Layer_Array) is
   begin
      This.Layers := new NTypes.Layer_Array(Layers'Range);
      This.Layers.all := Layers;
   end Set_Layers;
   
   
   -----------------------------------------------------------------------------
   -- Call the necessary functions to set up the neural network. Layers must be
   -- configured previously with Set_layers. Inputs must be read before.
   -----------------------------------------------------------------------------
   procedure Configure_Train (This : access Neural_Network) is
   begin
      This.Generate_Random_Bias;
      This.Generate_Random_Weights;
      This.Generate_Network;
   end Configure_Train;
   
   -----------------------------------------------------------------------------
   -- Call the necessary functions to set up the neural network. Layers must be
   -- configured previously with Set_layers. As inputs takes the weights file
   -- name.
   -----------------------------------------------------------------------------
   procedure Configure_Run (This : access Neural_Network; Weight_File : String) is
   begin
      This.Generate_Random_Bias;
      This.Generate_Random_Weights;
      This.Load_Weights(Weight_File);
      This.Generate_Network;
   end Configure_Run;
   
   
   -----------------------------------------------------------------------------
   -- Main training function for the network. Needs the number of iterarions and
   -- the learning rate for the network
   -----------------------------------------------------------------------------
   procedure Train (This : access Neural_Network; Iteration_Number: Natural; Learning_Rate : Float) is   
      Error : Float := 0.0;
      Execution_Count : Natural := 0;
   begin
      for j in 1.. Iteration_Number loop
         Error := 0.0;
         for i in 1 .. This.Input_Samples_Size loop
            This.Run(This.Inputs_Array(i).all);
            Error := Error + This.Total_Error(i);
            This.Backpropagation(i, Learning_Rate);
         end loop;
         Execution_Count := Execution_Count + 1;
         Ada.Text_IO.Put_Line("Count: [" & Execution_Count'Img & "/" &
                              Iteration_Number'Img & "] Error: " & Error'Img);
      end loop;
      Print_Train_Outputs(This);
   end Train;
   
   
   -----------------------------------------------------------------------------
   -- Generates random weights for every neuron in the network. Should be called
   -- in training and production mode.
   -----------------------------------------------------------------------------
   procedure Generate_Random_Weights (This : access Neural_Network) is
      Gen : Ada.Numerics.Float_Random.Generator;
   begin
      for L in This.Layers.all'Range loop  -- For each layer
         This.Layers.all(L).Weights := new NTypes.Neuron_Weights(1 .. This.Layers.all(L).Size, 1 .. This.Layers.all(L).Input_num);      
         for N in 1..This.Layers.all(L).Size loop  -- For each neuron in layer  
            for W in 1 .. This.Layers.all(L).Input_num loop  -- For each weigth in neuron
               This.Layers.all(L).Weights.all(N,W) := Ada.Numerics.Float_Random.Random(Gen);
            end loop;             
         end loop;
      end loop;  
   end Generate_Random_Weights;


   -----------------------------------------------------------------------------
   -- Generates random bias for every neuron in the network. Should be called
   -- in training and production mode.
   -- TODO: Bias implemented but not used by now, not taken in account on the 
   -- backpropagation algorithm, so are less than useless, by now is filled  
   -- with 0s. Neither have support for read from file or write to it at the end
   -----------------------------------------------------------------------------
   procedure Generate_Random_Bias (This : access Neural_Network) is
   begin
      for L in This.Layers.all'Range loop  -- For each layer
         This.Layers.all(L).Bias := new NTypes.Bias_Type(1 .. This.Layers.all(L).Size);      
         for N in 1..This.Layers.all(L).Size loop  -- For each neuron in layer  
            This.Layers.all(L).Bias.all(N) := 0.0;
         end loop;
      end loop;  
   end Generate_Random_Bias;
   
   
   -----------------------------------------------------------------------------
   -- Generate the neural network for run it. 
   -- This procedure fills the array of neurons with functions, inputs, bias 
   -- and weights.
   -----------------------------------------------------------------------------
   procedure Generate_Network (This : access Neural_Network) is
   begin
      for L in This.Layers.all'Range loop -- For each layer
         
         This.Net(L) := new NTypes.Net_layer(1 .. This.Layers.all(L).Size);
         
         for N in 1 .. This.Layers.all(L).Size loop -- For each neuron
            
            -- In the first  layer, the number of inputs for the neuron are equal
            -- to the number of inputs of the network
            if L = 1 then
               This.Net(L).all(N) := new NTypes.Neuron(This.Input_Number);
            else
               This.Net(L).all(N) := new NTypes.Neuron(This.Layers.all(L-1).Size);
            end if;
            -- Initialize the neuron functions
            Set_Funcs(N         => This.Net(L).all(N).all,
                      Body_Func => This.Layers.all(L).Body_Func,
                      Act_Func  => This.Layers.all(L).Act_Func);
            
            -- Set the neuron weights
            for W in 1..This.Layers.all(L).Input_num loop
               This.Net(L).all(N).all.In_Weight(W) := This.Layers.all(L).Weights.all(N, W); 
            end loop;

            This.Net(L).all(N).all.Bias := This.Layers.all(L).Bias.all(N);
         end loop;      
      end loop;
   end Generate_Network;
   
   
   -----------------------------------------------------------------------------
   -- Clean the neural network's neurons information.
   -- Needs to be filled fully again.
   -----------------------------------------------------------------------------
   procedure Clean_Network (This : access Neural_Network) is
   begin
      for L in This.Layers.all'Range loop      
         for N in 1..This.Layers.all(L).Size loop
            This.Net(L).all(N).Body_Out    := 0.0;
            This.Net(L).all(N).Output      := 0.0;
            This.Net(L).all(N).Inputs      := (others => 0.0);
         end loop;
      end loop;
   end Clean_Network;
   
   
   -----------------------------------------------------------------------------
   -- Run the neural network with the given parameters, setted previously by
   -- the Generate_Network procedure. This calculates the output values for
   -- the neural network
   -----------------------------------------------------------------------------
   procedure Run (This : access Neural_Network; Inputs: NTypes.Float_Array) is
   begin
      pragma Assert(Inputs'Length = This.Input_Number, "Input size error in run");
      for L in This.Layers.all'Range loop -- For each layer     
         -- Fill the neuron input data and execute it
         if L = 1 then -- First layer
            for N in 1 .. This.Layers.all(L).Size loop -- For each neuron
               Set_Inputs(N      => This.Net(L).all(N).all,
                          Inputs => Inputs);
               Exec_Neuron(This.Net(L).all(N).all);
            end loop;          
         else -- No first layer
            -- The inputs for the mid layers are the output of the last layer
            for N in 1..This.Layers.all(L).Size loop -- For each neuron
               for I in 1..This.Layers.all(L-1).Size loop -- For each neuron's input
                  This.Net(L).all(N).Inputs(I) := This.Net(L-1).all(I).Output;
               end loop;                  
               -- We execute each neuron
               Exec_Neuron(This.Net(L).all(N).all);
            end loop;
         end if;             
      end loop;
   end Run;
   
   
   -----------------------------------------------------------------------------
   -- Backpropagation algorithm. Should be only used for neural network training
   -- and must be called after every neural network run for a propper learning.
   -- This function modifies the neuron weights, NOT THE BIAS.
   -- Useful explanation for better understanding
   -- https://mattmazur.com/2015/03/17/a-step-by-step-backpropagation-example/
   -----------------------------------------------------------------------------
   procedure Backpropagation (This : access Neural_Network; Run_Number: Natural; Learning_Rate : Float) is
      
      procedure Process_Last_Weight_Variation (Layer : Natural; N : Natural; Origin : Natural) is
         Neur      : NTypes.Neuron := This.Net(Layer).all(N).all;        -- Current neuron to operate
         Neur_Orig : NTypes.Neuron := This.Net(Layer-1).all(Origin).all; -- Neuron of the last layer
         Target    : Float := This.Expected_Outputs_Array(Run_Number)(N); -- Expected output from the neuron
         Weight    : Float := Neur.In_Weight(Origin);                -- Weight we are going to evaluate
         --Input   : Float := Neur.Inputs(Origin);                   -- Input affected by the weight
         
         dEtot : Float := 0.0;
         dOut  : Float := 0.0;
         dNet  : Float := 0.0;
         dErr  : Float := 0.0;
      begin
         dEtot := Neur.Output - Target;                -- Derivate total error over output
         dOut  := Neur.Output * ( 1.0 - Neur.Output ); -- Derivate output over net input (body output)
         dNet  := Neur_Orig.Output;                    -- Derivate net input (body output) over weight
         dErr  := dEtot * dOut * dNet;          
         This.Net(Layer).all(N).all.In_Weight(Origin) := Weight - Learning_Rate * dErr;
         This.Net(Layer).all(N).all.dEtot := dEtot;
         This.Net(Layer).all(N).all.dOut  := dOut;
      end Process_Last_Weight_Variation;
      
      procedure Process_Hidden_Weight_Variation (Layer : Natural; N : Natural; Origin : Natural) is
         Neur    : NTypes.Neuron := This.Net(Layer).all(N).all;        -- Current neuron to operate
         Weight  : Float := Neur.In_Weight(Origin);                -- Weight we are going to evaluate
         --Input   : Float := Neur.Inputs(Origin);                   -- Input affected by the weight
         
         dEtot : Float := 0.0;
         dOut  : Float := 0.0;
         dNet  : Float := 0.0;
         dErr  : Float := 0.0;
      begin
         
         -- Calculation of dEtot
         for Neu in This.Net(Layer + 1)'Range loop -- We use the next layer and loop on the neurons
            declare
               dOut : Float := 0.0;
               dEo  : Float := 0.0;
               dNet : Float := 0.0;
               Next_Neur : NTypes.Neuron := This.Net(Layer+1).all(Neu).all;    -- Current neuron to operate
            begin
               dEo   := Next_Neur.dEtot * Next_Neur.dOut; -- We get derivatives calculated from the next layer neuron
               dNet  := Next_Neur.In_Weight(N);           -- We get the weight in which we are connected
               dOut  := dEo * dNet;
               dEtot := dEtot + dOut;                     -- Acummulate the calculated total error
            end;
         end loop;
                
         dOut  := Neur.Output * ( 1.0 - Neur.Output ); -- Derivate output over net input (body output)
         
         -- Derivate net input (body output) over weight
         if Layer = 1 then
            dNet := This.Inputs_Array(Run_Number)(Origin);
         else
            dNet := This.Net(Layer-1).all(Origin).all.Output;
         end if;                          
         dErr  := dEtot * dOut * dNet;          
         This.Net(Layer).all(N).all.In_Weight(Origin) := Weight - Learning_Rate * dErr;
         This.Net(Layer).all(N).all.dEtot := dEtot;
         This.Net(Layer).all(N).all.dOut  := dOut;
      end Process_Hidden_Weight_Variation;
      
   begin
      
      for L in reverse This.Layers.all'Range loop    -- For each layer in reverse loop
         for N in 1..This.Layers.all(L).Size loop    -- For each neuron in layer
            for W in This.Net(L).all(N).In_Weight'Range loop -- For each weight in neuron                      
               if L = This.Layers.all'Last then       -- Special treatment for last layer
                  Process_Last_Weight_Variation(L, N, W);
               else
                  Process_Hidden_Weight_Variation(L, N, W);
               end if;
               
            end loop;
         end loop;
      end loop;      
   end Backpropagation;
   
   
   -----------------------------------------------------------------------------
   -- Calculates the total error given by a neural network run, in comparison
   -- with the expected output values read in the input file
   -----------------------------------------------------------------------------
   function Total_Error (This : access Neural_Network; Run_Number: Natural) return Float is
      Total_Error    : Float := 0.0;
      Neuron_Error   : Float := 0.0;
      Expected_Value : Float := 0.0;
      Neuron_Output  : Float := 0.0;
   begin
      for i in 1 .. This.Output_Number loop
         Neuron_Output  := This.Net(This.Layers.all'Last).all(i).Output;
         Expected_Value := This.Expected_Outputs_Array(Run_Number)(i);
         Neuron_Error   := (( Expected_Value - Neuron_Output ) ** 2 ) / 2.0;
         Total_Error    := Total_Error + Neuron_Error;
      end loop;
      return Total_Error;
   end Total_Error;
   
   
   -----------------------------------------------------------------------------
   -- Calculates the error independent for every single output, in comparison
   -- with the expected output values read in the input file. Returns an array.
   -----------------------------------------------------------------------------
   function Partial_Error (This : access Neural_Network; Run_Number: Natural) return NTypes.Float_Array is
      Partial_Error  : NTypes.Float_Array(1 .. This.Output_Number) := (others => 0.0);
      Neuron_Error   : Float := 0.0;
      Expected_Value : Float := 0.0;
      Neuron_Output  : Float := 0.0;
   begin
      for i in 1 .. This.Output_Number loop
         Neuron_Output    := This.Net(This.Layers.all'Last).all(i).Output;
         Expected_Value   := This.Expected_Outputs_Array(Run_Number)(i);
         Neuron_Error     := (( Expected_Value - Neuron_Output ) ** 2 ) / 2.0;
         Partial_Error(i) := Neuron_Error;
      end loop;
      return Partial_Error;
   end Partial_Error;
   
   
   -----------------------------------------------------------------------------
   -- Sets the body and actuator functions for a neuron
   -----------------------------------------------------------------------------
   procedure Set_Funcs ( N:            in out NTypes.Neuron;
                         Body_Func:    in NTypes.Func;
                         Act_Func :    in NTypes.Func) is
   begin
      N.Body_Func := Body_Func;
      N.Act_Func  := Act_Func;
   end Set_Funcs;
      
   
   -----------------------------------------------------------------------------
   -- Executes the body and actuator functions for a neuron
   -----------------------------------------------------------------------------
   procedure Exec_Neuron ( N: in out NTypes.Neuron) is
   begin
      N.Body_Out := N.Body_Func.all(N);
      N.Output   := N.Act_Func.all(N);
   end Exec_Neuron;   

   
   -----------------------------------------------------------------------------
   -- Sets a neuron weights
   -----------------------------------------------------------------------------
   procedure Set_Weights ( N: in out NTypes.Neuron; 
                           Weights: in NTypes.Float_Array) is
   begin
      N.In_Weight := Weights;
   end Set_Weights;
   
   
   -----------------------------------------------------------------------------
   -- Sets a neuron inputs
   -----------------------------------------------------------------------------
   procedure Set_Inputs ( N: in out NTypes.Neuron; 
                          Inputs: in NTypes.Float_Array) is
   begin
      N.Inputs      := Inputs;
   end Set_Inputs;
   
   
   -----------------------------------------------------------------------------
   -- Read the input file given by config.adb and store it on a local array and
   -- calls Process_File to store it in the data structures
   -----------------------------------------------------------------------------
   procedure Read_Inputs (This : access Neural_Network; File : String) is
      Input    : Ada.Text_IO.File_Type;
      Max_size : Integer   := NTypes.Max_Input_File_Line_Size;
      CR       : Character := Ada.Characters.Latin_1.CR;
   begin
      begin
         Ada.Text_IO.Open(File => Input,
                          Mode => Ada.Text_IO.In_File,
                          Name => File);
      exception
         when others =>
            Ada.Text_IO.Put_Line("[ERROR] Cannot open input file");
            raise;
      end;
      for i in This.File_Lines'Range loop
         declare
            Str : String := Ada.Text_IO.Get_Line(Input) & CR;
         begin
            if Str'Length > Max_size then
               This.File_Lines(i) := Str(1 .. Max_size);
               Ada.Text_IO.Put_Line("Line " & I'Img & " in input file " & " exceeds the maximun length");
            elsif Str'Length < Max_size then
               This.File_Lines(i) := (others => ' ');
               This.File_Lines(i)(1..Str'Length) := Str;
            else
               This.File_Lines(i)   := Str;
            end if;           
         end;
      end loop;
      Ada.Text_IO.Close (Input);
      
      -- Processes the readed data 
      Process_File(This);
      
   exception
      when Ada.Text_IO.End_Error =>
         Ada.Text_IO.Put_Line("[ERROR] Input_Samples_Size not set properly on config, does not match with input file size (lines)");
         raise;
   end Read_Inputs;
   
   
   -----------------------------------------------------------------------------
   -- Writes the configuration and the weights obtained by the network learning
   -- into a file to be loaded in production mode. Lines begining with '#' are
   -- comments and are not read by the Load_Weights procedure
   -----------------------------------------------------------------------------
   procedure Write_Weights (This : access Neural_Network; File : String) is
      Output : Ada.Text_IO.File_Type;
      Aux    : Float;
   begin
      begin
         Ada.Text_IO.Create (File => Output,
                             Mode => Ada.Text_IO.Out_File,
                             Name => File);
      exception
         when others =>
            Ada.Text_IO.Put_Line("[ERROR] Cannot create output file");
            raise;
      end;      
      
      -- Print Config
      Ada.Text_IO.Put_Line(Output, "#Input_Samples_Size: " & This.Input_Samples_Size'Img);
      Ada.Text_IO.Put_Line(Output, "#Input_Number: " & This.Input_Number'Img);
      Ada.Text_IO.Put_Line(Output, "#");
      Ada.Text_IO.Put_Line(Output, "#Output_Number: " & This.Output_Number'Img);
      Ada.Text_IO.Put_Line(Output, "#");
      Ada.Text_IO.Put_Line(Output, "#Layer_Number: " & This.Layer_Number'Img);
      for L in This.Layers.all'Range loop
        Ada.Text_IO.Put_Line(Output, "#Layer " & L'Img & " size:" & This.Layers.all(L).Size'Img); 
      end loop;
      Ada.Text_IO.Put_Line(Output, "#");
      
      -- Print weights  
      Ada.Text_IO.Put_Line(Output, "Weights:");
      for L in This.Layers.all'Range loop    -- For each layer in reverse loop
         for N in 1..This.Layers.all(L).Size loop    -- For each neuron in layer
            for W in This.Net(L).all(N).In_Weight'Range loop -- For each weight in neuron
               Aux := This.Net(L).all(N).In_Weight(W);
               Ada.Float_Text_IO.Put(File => Output,
                                     Item => Aux,
                                     Fore => 3,
                                     Aft  => 5,
                                     Exp  => 0);
            end loop;
            Ada.Text_IO.New_Line(Output);
         end loop;
      end loop; 
      Ada.Text_IO.Close (Output);
   end Write_Weights;
   

   -----------------------------------------------------------------------------
   -- This function splits the source string by a Character, and returns the 
   -- range of the string which content position its equal to the field. If not 
   -- found, returns -1 in both type values. Field = 0 is the first match
   -----------------------------------------------------------------------------
   function Split( Source  : in String; 
                   Pattern : in Character;
                   Field   : in Natural) return NTypes.String_range_type is
      First  : Natural := 1;
      Count  : Natural := 0;
      Result : NTypes.String_range_type := (First => -1,
                                     Last  => -1);        
      CR       : Character := Ada.Characters.Latin_1.CR;
   begin
      for i in Source'Range loop
         if Source(i) = Pattern or   -- We found the pattern we are looking for   
            Source(i) = '-'     or   -- A special delimiter
            Source(i) = CR      or
            i = Source'Last     then -- Or the end      
            if Count = Field then        -- Its in the right index          
               if First <= i - 1 then    -- Has content
                  Result.First := First;
                  if i = Source'Last then 
                     Result.Last  := i; -- If we are in the end, the last character is the current
                  else
                     Result.Last  := i - 1; -- If not, take the previous character
                  end if;
                  return Result;
               else                      -- We found it, but its empty
                  return Result;
               end if;   
            end if;
            First := i + 1;              -- We must keep looping
            Count := Count + 1;
         end if;
      end loop;    
      return Result;                     -- Not found
   exception
      when others =>
         Ada.Text_IO.Put_Line("[ERROR] Split error, input data not properly formatted");
         raise;
   end Split;
   
   
   -----------------------------------------------------------------------------
   -- Reads the local array with the input file information and fill the inputs
   -- and outputs data for the network
   -----------------------------------------------------------------------------
   procedure Process_File  (This : access Neural_Network) is
      Input_Line    : NTypes.Float_Array(1 .. This.Input_Number);
      Output_Line   : NTypes.Float_Array(1 .. This.Output_Number);
      Inputs_Range  : NTypes.String_range_type;
      Outputs_Range : NTypes.String_range_type;
      String_Range  : NTypes.String_range_type;
      
      Temp: Natural := 0;
   begin
      for L in This.File_Lines'Range loop -- For each line
         
         -- Get the first chunck of the line, delimited by '-'
         Inputs_Range := Split(Source  => This.File_Lines(L),
                               Pattern => '-',
                               Field   => 0);
         
         -- Get the second chunck of the line
         Outputs_Range := Split(Source  => This.File_Lines(L),
                                Pattern => '-',
                                Field   => 1);     
         declare
            -- Take the input string with the calculated bounds 
            Inputs  : String(1 .. Inputs_Range.Last - Inputs_Range.First + 1) := 
                      This.File_Lines(L)(Inputs_Range.First .. Inputs_Range.Last);
            -- Take the output string with the calculated bounds 
            Outputs : String(1.. Outputs_Range.Last - Outputs_Range.First + 1) := 
                      This.File_Lines(L)(Outputs_Range.First .. Outputs_Range.Last);
         begin
            -- Inputs treatment
            for I in Input_Line'Range loop -- For each input value in line
               String_Range := Split(Source  => Inputs,
                                     Pattern => ',',
                                     Field   => I - 1); -- First field is 0
               if String_Range.First = -1 then
                  Ada.Text_IO.Put_Line("Line " & L'Img & " Index: " & I'Img & " input value not found");
               else
                  Input_Line(I) := Float'Value(Inputs(String_Range.First .. String_Range.Last));
               end if;
            end loop;
            This.Inputs_Array(L) := new NTypes.Float_Array(Input_Line'Range);
            This.Inputs_Array(L).all := Input_Line; -- We finally fill the inputs array for processing
            
            -- Outputs treatment
            for I in Output_Line'Range loop -- For each output value in line
               String_Range := Split(Source  => Outputs,
                                     Pattern => ',',
                                     Field   => I - 1); -- First field is 0
               if String_Range.First = -1 then
                  Ada.Text_IO.Put_Line("Line " & L'Img & " Index: " & I'Img & " output value not found");
               else
                  Output_Line(I) := Float'Value(Outputs(String_Range.First .. String_Range.Last));
               end if;
            end loop;
            This.Expected_Outputs_Array(L) := new NTypes.Float_Array(Output_Line'Range);
            This.Expected_Outputs_Array(L).all := Output_Line; -- We finally fill the outputs array for processing
         end;
      end loop;
   end Process_File;
   
   
   -----------------------------------------------------------------------------
   -- Loads the weight file given in config.adb and previously generated by the 
   -- Write_Outputs function. Used for production mode, not learning. Lines 
   -- begining with '#' are comments and are not processed.
   -----------------------------------------------------------------------------
   procedure Load_Weights (This : access Neural_Network; File : String) is
      Weights    : Ada.Text_IO.File_Type;
   begin
      begin
         Ada.Text_IO.Open(File => Weights,
                          Mode => Ada.Text_IO.In_File,
                          Name => File);
      exception
         when others =>
            Ada.Text_IO.Put_Line("[ERROR] Cannot open weights file");
            raise;
      end;
      
      -- Skip comments
      loop 
         declare
            Str : String := Ada.Text_IO.Get_Line(Weights);
         begin
            exit when Str(1) /= '#';
         end;         
      end loop;
            
      for L in This.Layers.all'Range loop
         for N in 1 .. This.Layers.all(L).Size loop
            declare
               Str   : String := Ada.Text_IO.Get_Line(Weights);
               First : Integer := 0;
               Last  : Integer := 0;     
            begin
               for W in 1 .. This.Layers.all(L).Input_num loop
                  -- We take each value formed with 9 fixed characters
                  First := 1 + ((W-1) * 9);
                  Last  := 9 + ((W-1) * 9);
                  This.Layers.all(L).Weights.all(N,W) := Float'Value(Str(First .. Last));
               end loop;
            end;
         end loop;
      end loop;      
      Ada.Text_IO.Close (Weights);      
   exception
      when Ada.Text_IO.End_Error =>
         Ada.Text_IO.Put_Line("[ERROR] Weights file empty");
         if Ada.Text_IO.Is_Open(Weights) then 
            Ada.Text_IO.Close (Weights);
         end if;  
         raise;
      when others =>
         Ada.Text_IO.Put_Line("[ERROR] Weights file doesn't match config sizes or layers weight" &
                              " array no generated (Call Network.Generate_Random_Weights first)");
         raise;
   end Load_Weights;
   
    
   -----------------------------------------------------------------------------
   -- Prints the output values for every sample of the input file 
   -- Useful to check the results of a training
   -----------------------------------------------------------------------------
   procedure Print_Train_Outputs (This : access Neural_Network) is 
      Aux  : Float   := 0.0;
      Last : Natural := This.Layers.all'Last;
   begin
      Ada.Text_IO.Put_Line("");
      for S in 1 .. This.Input_Samples_Size loop    -- For each sample
         Ada.Text_IO.Put_Line("");
         Ada.Text_IO.Put_Line("   Sample:" & S'Image);
         This.Run(This.Inputs_Array(S).all);
         for N in 1 .. This.Layers.all(Last).Size loop    -- For each neuron in the last layer
            Aux := This.Net(Last).all(N).Output;
            Ada.Text_IO.Put(Aux'Image & ",");
         end loop;
      end loop;
      Ada.Text_IO.Put_Line("");
   end Print_Train_Outputs;
   
   -----------------------------------------------------------------------------
   -- Prints the output of the last run
   -----------------------------------------------------------------------------
   procedure Print_Last_Output (This : access Neural_Network) is 
      Aux  : Float   := 0.0;
      Last : Natural := This.Layers.all'Last;
   begin
      Ada.Text_IO.Put_Line("");
      Ada.Text_IO.Put_Line("   Result:");
      for N in 1 .. This.Layers.all(Last).Size loop    -- For each neuron in the last layer
         Aux := This.Net(Last).all(N).Output;
         Ada.Text_IO.Put(Aux'Image & ",");
      end loop;
      Ada.Text_IO.Put_Line("");
   end Print_Last_Output;
   
   -----------------------------------------------------------------------------
   -- Returns the output of the last run as a Float array
   -----------------------------------------------------------------------------
   function Get_Output (This : access Neural_Network) return NTypes.Float_Array is 
      Aux  : Float   := 0.0;
      Last : Natural := This.Layers.all'Last;
      Ret  : NTypes.Float_Array(1 .. This.Layers.all(Last).Size);
   begin
      for N in 1 .. This.Layers.all(Last).Size loop    -- For each neuron in the last layer
         Ret(N) := This.Net(Last).all(N).Output;
      end loop;
      return Ret;
   end Get_Output;
   
   -----------------------------------------------------------------------------
   -- Prints the weigths of all neurons of all layers.
   -- Useful for check the backpropagation propper weight calculation
   -----------------------------------------------------------------------------
   procedure Print_Final_Weights (This : access Neural_Network) is 
      Aux : Float   := 0.0;
      L   : Natural := This.Layers.all'Last;
   begin
      Ada.Text_IO.Put_Line("");
      for L in reverse This.Layers.all'Range loop    -- For each layer in reverse loop
         Ada.Text_IO.Put_Line("  Layer: " & L'Image);
         for N in 1..This.Layers.all(L).Size loop    -- For each neuron in layer
            Ada.Text_IO.Put("(");
            for W in This.Net(L).all(N).In_Weight'Range loop -- For each weight in neuron
               Aux := This.Net(L).all(N).In_Weight(W);
               Ada.Float_Text_IO.Put(Item => Aux,
                                     Fore => 3,
                                     Aft  => 5,
                                     Exp  => 0);             
               Ada.Text_IO.Put(",");
            end loop;
            Ada.Text_IO.Put_Line(")");
         end loop;
         Ada.Text_IO.Put_Line(")");
      end loop;
      Ada.Text_IO.Put_Line("");
   end Print_Final_Weights;

   
   -----------------------------------------------------------------------------
   -- Open the console input for final network depuration. Accepts commands to
   -- see layer and neurons data. Format : L[00-99](optional)N[00-99]
   -- eg:  L02    -> Prints all the neurons on layer 2
   --      L02N07 -> Prints the neuron 7 of layer 2
   -----------------------------------------------------------------------------
   procedure Pint_Final_Data_Commands (This : access Neural_Network) is
      Str    : String(1..50) := (others => ' ');
      Last   : Integer := 0;
      Layer  : Integer := 0;
      Neuron : Integer := 0;
   begin
      loop
         Ada.Text_IO.Put_Line("");
         Ada.Text_IO.Put_Line("Command: L[00-99](optional)N[00-99]");
         Ada.Text_IO.Get_Line(Str, Last);
         if Str(1) = 'L' and Str(4) = 'N' then
            Layer  := Integer'Value(Str(2..3));
            Neuron := Integer'Value(Str(5..6));
            Print_Neuron(This, Layer, Neuron);
         elsif Str(1) = 'L' then
            Layer := Integer'Value(Str(2..3));
            Print_Layer(This, layer);
         else
            Ada.Text_IO.Put_Line("Unknown command");
         end if;
         delay 0.01;
      end loop;
   end Pint_Final_Data_Commands;
   
   
   -----------------------------------------------------------------------------
   -- Prints the neurons of a given layer
   -----------------------------------------------------------------------------
   procedure Print_Layer (This : access Neural_Network; L : Natural) is
   begin
      for N in 1 .. This.Layers.all(L).Size loop      
         Print_Neuron(This, L, N);
      end loop;
   end Print_Layer;
      
   
   -----------------------------------------------------------------------------
   -- Prints a neuron
   -----------------------------------------------------------------------------
   procedure Print_Neuron (This : access Neural_Network; L : Natural; N : Natural) is
      Neuron : NTypes.Neuron := This.Net(L).all(N).all;
   begin
      Ada.Text_IO.Put_Line("Neuron " & N'Img);
      Ada.Text_IO.Put("  Inputs: ");
      for I in Neuron.Inputs'Range loop
         Ada.Text_IO.Put(Neuron.Inputs(I)'Img & " ");
      end loop;
      Ada.Text_IO.New_Line;
      
      Ada.Text_IO.Put("  Weights: ");
      for I in Neuron.Inputs'Range loop
         Ada.Text_IO.Put(Neuron.In_Weight(I)'Img & " ");
      end loop;
      Ada.Text_IO.New_Line;
      
      Ada.Text_IO.Put_Line("  Bias: "     & Neuron.Bias'Img);
      Ada.Text_IO.Put_Line("  Body out: " & Neuron.Body_Out'Img);
      Ada.Text_IO.Put_Line("  Output: "   & Neuron.Output'Img);
      Ada.Text_IO.Put_Line("  dEtot: "    & Neuron.dEtot'Img);
      Ada.Text_IO.Put_Line("  dOut: "     & Neuron.dOut'Img);
      Ada.Text_IO.New_Line;
   end Print_Neuron;

end NeuralNetwork;
