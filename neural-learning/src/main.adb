with Config;
with NeuralNetwork;

procedure Main is

   Net : NeuralNetwork.Neural_Network_Access :=
     new NeuralNetwork.Neural_Network(Layer_Number       => Config.Layer_Number,
                                      Input_Number       => Config.Input_Number,
                                      Output_Number      => Config.Output_Number,
                                      Input_Samples_Size => Config.Input_Samples_Size);
begin

   Net.Set_Layers(Config.Layers);
   Net.Read_Inputs(Config.Input_File);
   Net.Configure_Train;
   Net.Train(20_000, Config.Learning_Rate);
   Net.Write_Weights(Config.Otput_File);
   Net.Print_Final_Weights;
   Net.Pint_Final_Data_Commands;

end Main;
