import serial
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import re
import threading
from scipy.signal import filtfilt, iirnotch, freqz, butter
from scipy.fftpack import fft, fftshift, fftfreq
import numpy as np
import scipy.fftpack

def butter_lowpass(cutoff, fs, order=5):
    nyq = 0.5 * fs
    normal_cutoff = cutoff / nyq
    b, a = butter(order, normal_cutoff, btype='low', analog=False)
    return b, a

gData = [0]

fig = plt.figure()
ax = fig.add_subplot(111)
hl, = ax.plot(range(len(gData)), gData)
plt.ylim(0, 3300)
plt.xlim(0,500)


f0 = 40
fs = 100
# Filtering using iirnotch
w0 = f0/(fs/2)
Q = 20
#b, a = iirnotch(w0, Q)
b, a = butter_lowpass(40, fs, 10)

# filter response
w, h = freqz(b, a)
filt_freq = w*fs/(2*np.pi)


def do_fft(y, fs):
    Y = fftshift(fft(y, 2 ** 12))
    f = fftshift(fftfreq(2 ** 12, 1 / fs))
    return f, Y


#python code
def filter_50(signal):
    for i in np.arange(50,500,50):
        fs = 1000.0  # Sample frequency (Hz)
        f0 = i  # Frequency to be removed from signal (Hz)
        w0 = f0 / (fs / 2)  # Normalized Frequency
        Q= 30
        b, a = iirnotch(w0, Q)
        signal = scipy.signal.filtfilt(b, a, signal)
    return(signal)


def GetData(out_data):
    with serial.Serial('\\.\COM9',115200, timeout=1) as ser:
        print(ser.isOpen())
        while True:
            line = ser.readline().decode('utf-8')

            if "ADC:" in line:
                data = line.split(" ")[-1]
                out_data.append(float(data))
                if len(out_data) > 500:
                    out_data.pop(0)
               


def update_line(num, hl, data):
    global filt_freq, ax2, b, a

    if len(data) > 50:
        y_filt = filtfilt(b, a, data)
    else:
        y_filt = data
    hl.set_data(list(range(1,len(data)+1)), y_filt)
    
    plt.ylim(min(data),max(data))

    return hl,


line_ani = animation.FuncAnimation(fig, update_line, fargs=(hl, gData),
                                   interval=50, blit=False)

dataCollector = threading.Thread(target = GetData, args=(gData,))
dataCollector.start()

plt.show()

dataCollector.join()
